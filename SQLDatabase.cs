﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Manitou.Framework.Data
{
    /// <summary>
    /// SQLDatabase helper class.
    /// </summary>
    public static class SQLDatabase
    {
        #region Public Methods and Operators

        /// <summary>
        /// The add parameter.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        /// <param name="parameterName">
        /// The parameter name.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The command with the added parameter.
        /// </returns>
        public static SqlCommand AddParameter(this SqlCommand command, string parameterName, object value)
        {
            command.Parameters.Add(new SqlParameter(parameterName, value));
            return command;
        }

        /// <summary>
        /// The create command.
        /// </summary>
        /// <param name="connectionStringName">
        /// The connection string name.
        /// </param>
        /// <param name="commandType">
        /// The command type.
        /// </param>
        /// <param name="commandText">
        /// The command text.
        /// </param>
        /// <returns>
        /// A new SqlCommand.
        /// </returns>
        public static SqlCommand CreateCommand(string connectionStringName, CommandType commandType, string commandText)
        {
            var connectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
            var connection = new SqlConnection(connectionString);
            var command = new SqlCommand(commandText, connection) { CommandType = commandType };
            return command;
        }

        /// <summary>
        /// The execute non query.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        public static void ExecuteNonQuery(SqlCommand command)
        {
            using (var connection = command.Connection)
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// The execute reader.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        /// <returns>
        /// A list of results (dictionary(string, object))
        /// </returns>
        public static IList<Dictionary<string, object>> ExecuteReader(SqlCommand command)
        {
            var result = new List<Dictionary<string, object>>();
            using (var connection = command.Connection)
            {
                connection.Open();
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    var dictionary = new Dictionary<string, object>();
                    for (var i = 0; i < reader.FieldCount; i++)
                    {
                        dictionary.Add(reader.GetName(i), reader.GetValue(i));
                    }

                    result.Add(dictionary);
                }

                reader.Close();
            }

            return result;
        }

        /// <summary>
        /// The execute scalar.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        /// <returns>
        /// The scalar object returned after executing the command.
        /// </returns>
        public static object ExecuteScalar(SqlCommand command)
        {
            object result;
            using (var connection = command.Connection)
            {
                connection.Open();
                result = command.ExecuteScalar();
            }

            return result;
        }

        #endregion
    }
}