﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace Manitou.Framework
{
    /// <summary>
    /// Defines a base data contract to be used for all data contracts.
    /// </summary>
    [DataContract]
    public class BaseDataContract : INotifyPropertyChanged
    {
        #region Public Events

        /// <summary>
        /// The property changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The on property changed.
        /// </summary>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged == null)
            {
                return;
            }

            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public void SetProperty<T>(ref T property, object value, string propertyName)
        {
            var castedValue = (T)value;
            if (property is int || property is bool || property is byte || property is char || property is decimal
                || property is double || //property is enum ||
                property is float || property is long || property is sbyte || property is short
                || //property is struct ||
                property is uint || property is ulong || property is ushort)
            {
                if (property.Equals(castedValue))
                {
                    return;
                }
                property = castedValue;
                OnPropertyChanged(propertyName);
            }
            else
            {
                if (property != null && property.Equals(castedValue))
                {
                    return;
                }
                property = castedValue;
                OnPropertyChanged(propertyName);
            }
        }

        #endregion
    }
}