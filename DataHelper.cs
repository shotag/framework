﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Reflection;

namespace Manitou.Framework
{
    public static class DataHelper
    {
        public static IList<T> ReaderAndMap<T>(this Dictionary<string, Tuple<string, Func<string, object>>> dictionary, DbCommand command)
        {
            var properties = GetProperties<T>();
            var result = new List<T>();
            command.Connection.Open();
            var reader = command.ExecuteReader();
            while (reader.Read())
            {
                var obj = Activator.CreateInstance<T>();
                dictionary.ToList().ForEach(d => {
                    if (!string.IsNullOrWhiteSpace(d.Value.Item1)){
                        var property = properties.First(p => p.Name == d.Key);
                        var temp = reader[d.Value.Item1];
                        if (d.Value.Item2 != null)
                        {
                            temp = d.Value.Item2(reader[d.Value.Item1].ToString());
                        }
                        property.SetValue(obj, Convert.ChangeType(temp, property.PropertyType), null);
                    }
                });
                result.Add(obj);
            }
            command.Connection.Close();
            return result;
        }

        public static Dictionary<string, Tuple<string, Func<string, object>>> Map<T>()
        { 
            var properties = GetProperties<T>();
            var result = new Dictionary<string, Tuple<string, Func<string, object>>>();
            properties.ToList().ForEach(p => result.Add(p.Name, new Tuple<string, Func<string, object>>(p.Name, null)));
            return result;
        }

        public static Dictionary<string, Tuple<string, Func<string, object>>> NoMap<T>()
        {
            var properties = GetProperties<T>();
            var result = new Dictionary<string, Tuple<string, Func<string, object>>>();
            properties.ToList().ForEach(p => result.Add(p.Name, new Tuple<string, Func<string, object>>(null, null)));
            return result;
        }

        public static Dictionary<string, Tuple<string, Func<string, object>>> RemapPropertyColumn(this Dictionary<string, Tuple<string, Func<string, object>>> dictionary, string propertyName, string columnName)
        {
            return RemapPropertyColumnAndFunction(dictionary, propertyName, columnName, dictionary[propertyName].Item2);
        }

        public static Dictionary<string, Tuple<string, Func<string, object>>> RemapPropertyFunction(this Dictionary<string, Tuple<string, Func<string, object>>> dictionary, string propertyName, Func<string, object> function)
        {
            return RemapPropertyColumnAndFunction(dictionary, propertyName, dictionary[propertyName].Item1, function);
        }

        public static Dictionary<string, Tuple<string, Func<string, object>>> RemapPropertyColumnAndFunction(this Dictionary<string, Tuple<string, Func<string, object>>> dictionary, string propertyName, string columnName, Func<string, object> function)
        {
            dictionary[propertyName] = new Tuple<string, Func<string, object>>(columnName, function);
            return dictionary;
        }

        public static Dictionary<string, Tuple<string, Func<string, object>>> RemovePropertyMapping(this Dictionary<string, Tuple<string, Func<string, object>>> dictionary, string propertyName)
        {
            dictionary[propertyName] = new Tuple<string, Func<string, object>>(null, null);
            return dictionary;
        }

        public static DbCommand AddParams<T>(this Dictionary<string, string> dictionary, DbCommand command, T obj)
        {
            var properties = GetProperties<T>();
            dictionary.ToList().ForEach(d =>
                {
                    var param = command.CreateParameter();
                    param.ParameterName = d.Key;
                    param.Value = properties.First(p => p.Name == d.Key).GetValue(obj, null);
                    command.Parameters.Add(param);
                });
            return command;
        }

        private static IEnumerable<PropertyInfo> GetProperties<T>()
        {
            var type = typeof(T);
            var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance).ToList();
            return properties;
        }
    }
}
