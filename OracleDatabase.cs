﻿using System.Configuration;
using System.Data;

using Oracle.ManagedDataAccess.Client;

namespace Manitou.Framework.Data
{
    /// <summary>
    /// The oracle database.
    /// </summary>
    public static class OracleDatabase
    {
        #region Public Methods and Operators

        /// <summary>
        /// The add parameter.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        /// <param name="parameterName">
        /// The parameter name.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// An updated OracleCommand
        /// </returns>
        public static OracleCommand AddInParameter(this OracleCommand command, string parameterName, object value)
        {
            command.Parameters.Add(new OracleParameter(parameterName, value));
            return command;
        }

        public static OracleParameter CreateOutParameter(
            string parameterName, OracleDbType valueType)
        {
            var command = new OracleParameter
                { Direction = ParameterDirection.Output, OracleDbType = OracleDbType.RefCursor, ParameterName = parameterName};
            return command;
        }

        /// <summary>
        /// The create command.
        /// </summary>
        /// <param name="connectionStringName">
        /// The connection string name.
        /// </param>
        /// <param name="commandType">
        /// The command type.
        /// </param>
        /// <param name="commandText">
        /// The command text.
        /// </param>
        /// <returns>
        /// A new OracleCommand
        /// </returns>
        public static OracleCommand CreateCommand(
            string connectionStringName, CommandType commandType, string commandText)
        {
            var connectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
            var connection = new OracleConnection(connectionString);
            var command = new OracleCommand(commandText, connection) { CommandType = commandType };
            return command;
        }

        /// <summary>
        /// The fill dataset.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        /// <returns>
        /// A new dataset
        /// </returns>
        public static DataSet FillDataset(OracleCommand command)
        {
            var ds = new DataSet();
            var adapter = new OracleDataAdapter();
            command.Connection.Open();
            adapter.SelectCommand = command;
            adapter.Fill(ds);
            command.Connection.Close();
            return ds;
        }

        #endregion
    }
}